package com.deloitte.lineage.CommonMethods;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;



public class DriverInit {
	
	
	/**
	 * Manage WebDriver initialization before running tests.
	 * 
	 * @author Shweta Parlikar
	 * 
	 */

	
	private static final ThreadLocal<WebDriver> threadDriver = new ThreadLocal<>();
	private static final Logger LOGGER = LoggerFactory
			.getLogger(WebDriverFunctions.class);
	public static int TIMEOUT = 30; // seconds

	@BeforeSuite(alwaysRun = true)
	public void beforeSuite(String browser) {

		System.out.println("Executing Before Suite..");
		switch (browser) { 
		case "Firefox":
			System.out.println("Broswer is Firefox");
			initFirefoxDriver();
			break;

		case "Chrome":
			System.out.println("Broswer is Chrome");
			System.setProperty(
					"webdriver.chrome.driver",
					System.getProperty("user.dir")
							+ "//src//main//java//resources//chromedriver.exe");
			initChromeDriver();
			break;

		case "IE":
			System.out.println("Broswer is Internet Explorer");
			File ieFile = new File("C:/SeleniumDrivers/iexplorerdriver.exe");
			System.setProperty("webdriver.ie.driver", ieFile.getAbsolutePath());
			initInternetExplorerDriver();
			break;
			
		case "HTMLUnit" :
			System.out.println("Using HTML Unit code ");
			 DesiredCapabilities capabilities = DesiredCapabilities.htmlUnit();
             capabilities.setBrowserName("htmlunit");
             capabilities.setVersion("firefox");
             capabilities.setPlatform(Platform.ANY);
             capabilities.setJavascriptEnabled(true);
			initHtmlUnitDriver(capabilities);
		
			break;
			
		default:
			System.out.println("Default driver is Firefox Driver");
			initFirefoxDriver();
			break;

		}
	}

	public static WebDriver getDriver() {

		return threadDriver.get();
	}

	public static void setWebDriver(WebDriver webDriver) {
		threadDriver.set(webDriver);
	}

	/**
	 * Set Default timeout for WebDriver
	 * 
	 * @param timeout
	 *            - seconds to wait web element
	 */
	
	public static void setDefaultTimeout(int timeout) {
		TIMEOUT = timeout;
	}

	/**
	 * Set Wait and Script timeouts for WebDriver
	 * 
	 * @param timeout
	 *            - seconds to wait element and running script
	 */
	public static void setTimeout(int timeout) {
		DriverInit.getDriver().manage().timeouts()
				.implicitlyWait(timeout, TimeUnit.SECONDS);
		DriverInit.getDriver().manage().timeouts()
				.setScriptTimeout(timeout, TimeUnit.SECONDS);
	}

	/**
	 * Set Wait and Script timeouts for WebDriver
	 * 
	 * @param timeout
	 *            - milliseconds to wait element and running script
	 */
	public static void setTimeoutMls(int timeout) {
		DriverInit.getDriver().manage().timeouts()
				.implicitlyWait(timeout, TimeUnit.MILLISECONDS);
		DriverInit.getDriver().manage().timeouts()
				.setScriptTimeout(timeout, TimeUnit.MILLISECONDS);
	}

	/**
	 * initialization RemoteWebDriver
	 * 
	 * @param remoteUrl
	 *            - remote host
	 * @param capabilities
	 *            - desired capabilities
	 * @throws MalformedURLException
	 *             - exception
	 */
	public static void initRemoteWebDriver(String remoteUrl,
			Capabilities capabilities) throws MalformedURLException {
		LOGGER.info(String.format(
				"Initialization Remote Web Driver at url '%s'", remoteUrl));
		setWebDriver(new RemoteWebDriver(new URL(remoteUrl), capabilities));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization FirefoxDriver
	 * 
	 * @param capabilities
	 *            - desired capabilities
	 */
	public static void initFirefoxDriver(Capabilities capabilities) {
		LOGGER.info("Initialization Firefox Driver");
		setWebDriver(new FirefoxDriver(capabilities));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization ChromeDriver
	 * 
	 * @param capabilities
	 *            - desired capabilities
	 */
	public static void initChromeDriver(Capabilities capabilities) {
		LOGGER.info("Initialization Chrome Driver");
		setWebDriver(new ChromeDriver(capabilities));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization SafariDriver
	 * 
	 * @param capabilities
	 *            - desired capabilities
	 */
	public static void initSafariDriver(Capabilities capabilities) {
		LOGGER.info("Initialization Safari Driver");
		setWebDriver(new SafariDriver(capabilities));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization InternetExplorerDriver
	 * 
	 * @param capabilities
	 *            - desired capabilities
	 */
	public static void initInternetExplorerDriver(Capabilities capabilities) {
		LOGGER.info("Initialization Internet Explorer Driver");
		setWebDriver(new InternetExplorerDriver(capabilities));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization HtmlUnitDriver
	 * 
	 * @param capabilities
	 *            - desired capabilities
	 */
	public static void initHtmlUnitDriver(Capabilities capabilities) {
		LOGGER.info("Initialization Html Unit Driver");
		setWebDriver(new HtmlUnitDriver(capabilities));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization FF with some profile Use it if you want to use your
	 * profile for FF. It doesn't work remotely. Before running create your
	 * profile. Use cmd : firefox.exe -ProfileManager -no-remote
	 * 
	 * @param path
	 *            - profile path
	 */
	public static void initFFProfile(String path) {
		LOGGER.info(String.format(
				"Initialization Firefox Driver with Profile '%s'", path));
		File profileDir = new File(path);
		FirefoxProfile ffprofile = new FirefoxProfile(profileDir);
		ffprofile.setEnableNativeEvents(true);
		setWebDriver(new FirefoxDriver(ffprofile));
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization FirefoxDriver
	 */
	public static void initFirefoxDriver() {
		LOGGER.info("Initialization Firefox Driver");
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAcceptUntrustedCertificates(true);
		profile.setAssumeUntrustedCertificateIssuer(true);
		profile.setEnableNativeEvents(true);
		profile.setPreference("javascript.enabled", true);
		profile.setPreference("dom.max_script_run_time", 0);
		profile.setPreference("dom.max_chrome_script_run_time", 0);
		setWebDriver(new FirefoxDriver(profile));
		//setWebDriver(new FirefoxDriver(profile));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization InternetExplorerDriver
	 */
	public static void initInternetExplorerDriver() {
		LOGGER.info("Initialization Internet Explorer Driver");
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setPlatform(Platform.WINDOWS);
		capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		capabilities.setCapability(CapabilityType.HAS_NATIVE_EVENTS, true);
		capabilities.setJavascriptEnabled(true);
		capabilities
				.setCapability(
						InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
						true);
		capabilities.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING,
				true);
		capabilities.setCapability(
				InternetExplorerDriver.IE_ENSURE_CLEAN_SESSION, true);
		setWebDriver(new InternetExplorerDriver(capabilities));
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	/**
	 * initialization ChromeDriver
	 * 
	 */
	public static void initChromeDriver() {
		LOGGER.info("Initialization Chrome Driver");
		ChromeOptions options = new ChromeOptions();
		options.addArguments(Arrays.asList("--start-maximized", "--test-type",
				"--ignore-certificate-errors", "--disable-popup-blocking",
				"--allow-running-insecure-content", "--disable-translate",
				"--always-authorize-plugins"));
		setWebDriver(new ChromeDriver(options));
		//setTimeout(TIMEOUT);
	}

	/**
	 * initialization SafariDriver
	 */
	public static void initSafariDriver() {
		LOGGER.info("Initialization Safari Driver");
		setWebDriver(new SafariDriver());
		setTimeout(TIMEOUT);
		getDriver().manage().window().maximize();
	}

	@AfterSuite
	public void afterSuite() {
		System.out.println("After Suite...");
		getDriver().quit();
	}

}
