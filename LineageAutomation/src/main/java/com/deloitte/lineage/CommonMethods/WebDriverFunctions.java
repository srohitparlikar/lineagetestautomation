package com.deloitte.lineage.CommonMethods;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public final class WebDriverFunctions {

	/**
	 * Manage WebDriver instances.
	 * 
	 * @author Shweta Parlikar
	 * 
	 */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(WebDriverFunctions.class);
	
	private WebDriverFunctions() {
	}

	public static int TIMEOUT = 30; // seconds
	public static boolean CHECKCONDITION = true;

	
	/**
	 * Set Default check condition for wait methods
	 * 
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void setDefaultCheckCondition(boolean checkCondition) {
		CHECKCONDITION = checkCondition;
	}

	

	/**
	 * Remove all cookies from browser
	 */
	public static void deleteAllCookies() {
		LOGGER.info("Remove all cookies");
		DriverInit.getDriver().manage().deleteAllCookies();
	}

	/**
	 * Delete the named cookie from the current domain. This is equivalent to
	 * setting the named cookie's expiry date to some time in the past.
	 * 
	 * @param cookieName
	 *            The name of the cookie to delete
	 */
	public static void deleteCookieNamed(String cookieName) {
		LOGGER.info(String.format("Remove cookie: %s", cookieName));
		getCookieNamed(cookieName);
		DriverInit.getDriver().manage().deleteCookieNamed(cookieName);
		getCookieNamed(cookieName);
	}

	/**
	 * Log cookie.
	 * 
	 * @param cookieName
	 *            The name of the cookie to log.
	 */
	public static void getCookieNamed(String cookieName) {
		LOGGER.info(String.format("getCookieNamed: %s", cookieName));
		if ((DriverInit.getDriver().manage().getCookieNamed(cookieName)) != null) {
			LOGGER.info(String.format(" cookie: %s%s", cookieName, DriverInit.getDriver().manage().getCookieNamed(cookieName)));
		} else {
			LOGGER.info(String.format(" cookie: [ %s ] = null !!! )", cookieName));
		}
	}

	/**
	 * Open web page
	 * 
	 * @param page
	 *            - target for navigate
	 */
	public static void open(String page) {
		LOGGER.info(String.format("Navigate to page: %s",page));
		DriverInit.getDriver().get(page);
	}

	/**
	 * Quit from WebDriver (Closes all browser windows and safely ends the
	 * session).
	 */
	public static void quit() {
		if (DriverInit.getDriver() != null) {
			LOGGER.info("WebDriver Quit");
			DriverInit.getDriver().quit();
		}
	}

	/**
	 * Close the WebDriver (Close the browser window that the driver has focus
	 * of).
	 */
	public static void close() {
		if (DriverInit.getDriver() != null) {
			LOGGER.info("WebDriver Close");
			DriverInit.getDriver().close();
		}
	}

	/**
	 * Execute JavaScript code.
	 * 
	 * @param script
	 *            - target for execution
	 * @return Execution results
	 */
	public static Object executeScript(String script) {
		LOGGER.info(String.format("Execute Script - %s", script));
		return ((JavascriptExecutor) DriverInit.getDriver()).executeScript(script);
	}

	/**
	 * Switch to frame.
	 * 
	 * @param frame
	 *            - target frame
	 */
	public static void switchToFrame(String frame) {
		LOGGER.info(String.format("Switch To Frame - %s", frame));
		DriverInit.getDriver().switchTo().frame(frame);
	}

	/**
	 * Switch to window.
	 * 
	 * @param window
	 *            - target window
	 */
	public static void switchToWindow(String window) {
		LOGGER.info(String.format("Switch To Window - %s", window));
		DriverInit.getDriver().switchTo().window(window);
	}

	/**
	 * Switch to active element.
	 * 
	 * @return active WebElement
	 */
	public static WebElement switchToActive() {
		LOGGER.info("Switch To Active");
		return DriverInit.getDriver().switchTo().activeElement();
	}

	/**
	 * Switch to the next window.
	 */
	public static void switchWindow() {
		LOGGER.info("Switch Window");
		Set<String> handles = DriverInit.getDriver().getWindowHandles();
		if (handles.size() > 1) {
			String current = DriverInit.getDriver().getWindowHandle();
			handles.remove(current);
		} else {
			LOGGER.info("SwitchWindow: only one windows is available");
		}
		String newTab = handles.iterator().next();
		DriverInit.getDriver().switchTo().window(newTab);
	}

	/**
	 * Close active and switch to the next window.
	 * @throws InterruptedException 
	 */
	public static void switchCloseWindow() throws InterruptedException {
		LOGGER.info("Switch And Close Window");
		new WebDriverWait(DriverInit.getDriver(), TIMEOUT) {
		}.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver wDriver) {
				return (wDriver.getWindowHandles().size() > 1);
			}
		});
		Set<String> handles = DriverInit.getDriver().getWindowHandles();
		if (handles.size() == 1) {
			LOGGER.info("switchCloseWindow: only one windows is available");
			return;
		}
		DriverInit.getDriver().close();
		Thread.sleep(500);
		handles = DriverInit.getDriver().getWindowHandles();
		DriverInit.getDriver().switchTo().window(handles.iterator().next());
	}

	/**
	 * Switch to the default window.
	 */
	public static void switchToDefault() {
		LOGGER.info("Switch To Default");
		DriverInit.getDriver().switchTo().defaultContent();
	}

	/**
	 * Switch to the window with title.
	 * 
	 * @param title
	 *            - Expected active window title.
	 * @return True if window is switched, otherwise False
	 */
	public static boolean switchToWindowUsingTitle(String title) {
		LOGGER.info(String.format("Switch To Window Using Title '%s'", title));
		Set<String> availableWindows = DriverInit.getDriver().getWindowHandles();
		if (!availableWindows.isEmpty() && availableWindows.contains(title)) {
			DriverInit.getDriver().switchTo().window(title);
			return true;
		}
		return false;
	}

	/**
	 * Get active window handle.
	 * 
	 * @return Active window handle.
	 */
	public static String getWindowHandle() {
		LOGGER.info("Get Window Handle");
		return DriverInit.getDriver().getWindowHandle();
	}

	/**
	 * Get windows handles.
	 * 
	 * @return Window handles.
	 */
	public static Set<String> getWindowHandles() {
		LOGGER.info("Get Windows Handles");
		return DriverInit.getDriver().getWindowHandles();
	}

	/**
	 * Get window title.
	 * 
	 * @return Window title.
	 */
	public static String getWindowTitle() {
		LOGGER.info("Get Window Title");
		return DriverInit.getDriver().getTitle();
	}

	/**
	 * Click "go Back" button on the current page
	 */
	public static void goBack() {
		LOGGER.info("Go Back");
		DriverInit.getDriver().navigate().back();
	}

	/**
	 * Click "go Forward" button on the current page
	 */
	public static void goForward() {
		LOGGER.info("Go Forward");
		DriverInit.getDriver().navigate().forward();
	}

	/**
	 * Click "Refresh" button on the current page
	 */
	public static void refresh() {
		LOGGER.info("Refresh");
		DriverInit.getDriver().navigate().refresh();
	}

	/**
	 * Sleep by seconds.
	 * 
	 * @param seconds
	 *            to sleeping.
	 * @throws InterruptedException 
	 */
	public static void sleep(int seconds) throws InterruptedException {
		LOGGER.info(String.format("Sleeping %d seconds", seconds));
		Thread.sleep(10000);
	}

	/**
	 * Scroll page to top by JS
	 */
	public static void scrollPageToTop() {
		LOGGER.info("Scroll Page To Top");
		WebDriverFunctions.executeScript("window.scrollTo(0, 0);");
	}

	/**
	 * Scroll page down by JS
	 * 
	 * @param iHeight
	 *            - height
	 */
	public static void scrollPageDown(int iHeight) {
		LOGGER.info("Scroll Page To Top");
		WebDriverFunctions
				.executeScript("window.scrollTo(0, " + iHeight + ");");
	}

	/**
	 * Scroll page up by JS
	 * 
	 * @param iHeight
	 *            - height
	 */
	public static void scrollPageUp(int iHeight) {
		LOGGER.info("Scroll Page To Top");
		WebDriverFunctions
				.executeScript("window.scrollTo(0, " + iHeight + ");");
	}

	/**
	 * Scroll page to end by JS
	 */
	public static void scrollPageToEnd() {
		LOGGER.info("Scroll Page To End");
		WebDriverFunctions
				.executeScript("window.scrollTo(0, document.body.clientHeight )");
	}

}
