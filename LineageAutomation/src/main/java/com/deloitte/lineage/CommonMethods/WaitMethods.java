package com.deloitte.lineage.CommonMethods;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.apache.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.deloitte.lineage.CommonMethods.LocatorFactory.LOCATOR_TYPE;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class WaitMethods {
	/**
	 * Manage WebDriver instances.
	 * 
	 * @author Shweta Parlikar
	 * 
	 */
	private static WebDriver driver = DriverInit.getDriver();
	/*
	 * public WaitMethods() { this.driver = }
	 */

	private static Logger LOGGER = Logger.getLogger(WaitMethods.class);

	/**
	 * Waits for presence of an element
	 * 
	 * @param locator
	 *            locator of the element
	 * @return return the found WebElement
	 */
	public static boolean waitForElementPresent(By locator) {

		boolean found = false;
		WebElement we = null;
		final long startTime = System.currentTimeMillis();
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(
				DriverInit.getDriver()).withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class)
				.ignoring(NoSuchElementException.class);

		while ((System.currentTimeMillis() - startTime) < 91000)

		{
			try {
				we = wait.until(ExpectedConditions
						.presenceOfElementLocated(locator));
				if (we != null) {
					found = true;
					break;
				}
			} catch (StaleElementReferenceException stale) {
				LOGGER.info(
						"StaleElement Element reference exception thrown while waiting for locator.");
				return false;
			} catch (NoSuchElementException noSuch) {
				LOGGER.info("No Such Element exception thrown while waiting for locator");
				return false;
			}

			long endTime = System.currentTimeMillis();
			long totalTime = endTime - startTime;
			LOGGER.info("Total time taken is -----{}. ");

		}

		return found;
	}

	/**
	 * Waits for element to be visible
	 * 
	 * @param locator
	 *            locator of the element
	 */
	public static boolean waitForElementVisible(By locator) {
		boolean found = false;
		
		WebElement we = null;

		final long startTime = System.currentTimeMillis();
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(
				DriverInit.getDriver()).withTimeout(30, TimeUnit.SECONDS)
				.pollingEvery(5, TimeUnit.SECONDS)
				.ignoring(StaleElementReferenceException.class)
				.ignoring(NoSuchElementException.class);

		while ((System.currentTimeMillis() - startTime) < 91000)

		{
			try {
				we = wait.until(ExpectedConditions
						.visibilityOfElementLocated(locator));

				if (we != null) {
					found = true;
					break;
				}
			} catch (StaleElementReferenceException stale) {
				LOGGER.info(
						"StaleElement Element reference exception thrown while waiting for WebElement {}");
				return false;
			} catch (NoSuchElementException noSuch) {
				LOGGER.info("No Such Element exception thrown while waiting for WebElement");
				return false;
			}
		}
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		LOGGER.info("Total time taken is ----- "+String.valueOf(totalTime));

		return found;

	}

	/**
	 * Method to return an element with wait
	 * 
	 * @param locator
	 *            locator of the element
	 */
	public static WebElement getElementWithWait(By locator) {
		WebElement we = null;
		boolean found = false;
		long startTime = System.currentTimeMillis();
		while ((System.currentTimeMillis() - startTime) < 91000) {
			try {
				FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
						.withTimeout(30, TimeUnit.SECONDS)
						.pollingEvery(5, TimeUnit.SECONDS)
						.ignoring(StaleElementReferenceException.class)
						.ignoring(NoSuchElementException.class);
				we = wait.until(ExpectedConditions
						.visibilityOfElementLocated(locator));
				found = true;
			} catch (NoSuchElementException nse) {
				LOGGER.info("No Such elemment Exception occured while locating "
						+ locator);
				found = false;
			} catch (StaleElementReferenceException stale) {
				LOGGER.info(
						"Stale Element Reference Exception occured while locating {} ");
				found = false;
			}
		}
		long endTime = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println("Total time taken waiting for element " + totalTime);
		LOGGER.info("Total time taken waiting for element {}");
		if (found = true) {
			return we;
		}

		else {
			return null;
		}
	}

	public static WebElement getElement(By locator) {
		WebElement we = null;

		if (locator == null) {
			throw new IllegalArgumentException(
					"The locator cannot be null cannot be null.");
		}

		try {
			we = driver.findElement(locator);

		} catch (Exception e) {
			LOGGER.warn("Getting element {} threw exception {} ", e);

		}
		return we;
	}

	
	/*
	 * public static WebElement getElementWithWait(By locator) { if (locator ==
	 * null) { throw new
	 * IllegalArgumentException("The browserElement cannot be null."); }
	 * WebElement result = null; try { Wait<WebDriver> wait = new
	 * FluentWait<WebDriver>(DriverInit.getDriver()) .withTimeout(30,
	 * TimeUnit.MILLISECONDS) .pollingEvery(100L, TimeUnit.MILLISECONDS)
	 * .ignoring(NoSuchElementException.class);
	 * 
	 * result = (WebElement)wait.until(ExpectedConditions
	 * .visibilityOfElementLocated(locator)); } catch (TimeoutException e) {
	 * //LOGGER
	 * .warn("Could not find element '{}' located by '{}' within {} ms.", new
	 * Object[] { browserElement.getElementName(), browserElement.getLocator(),
	 * Integer.valueOf(browserElement.getTimeout()) }); } return result; }
	 */

	public static int TIMEOUT = 5; // seconds
	public static boolean CHECKCONDITION = true;

	/**
	 * Set Default check condition for wait methods
	 * 
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void setDefaultCheckCondition(boolean checkCondition) {
		CHECKCONDITION = checkCondition;
	}

	public static boolean waitWindowsCount(final int numberOfWindows) {
		LOGGER.info("Wait for Open Windows Count");
		new WebDriverWait(DriverInit.getDriver(), TIMEOUT) {
		}.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver wDriver) {
				return (wDriver.getWindowHandles().size() == numberOfWindows);
			}
		});
		return false;
	}

	/**
	 * Wait until all windows not empty.
	 * 
	 * @param numberOfWindows
	 *            - number of existing windows
	 * @throws InterruptedException
	 */
	public static void waitAllWindowsFullLoaded(final int numberOfWindows)
			throws InterruptedException {
		LOGGER.info("Wait for All Windows Full Loaded");
		new WebDriverWait(DriverInit.getDriver(), TIMEOUT) {
		}.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver wDriver) {
				return (wDriver.getWindowHandles().size() == numberOfWindows);
			}
		});

		new WebDriverWait(DriverInit.getDriver(), TIMEOUT) {
		}.until(new ExpectedCondition<Boolean>() {
			@Override
			public Boolean apply(WebDriver wDriver) {
				boolean fullLoaded = true;
				for (String window : wDriver.getWindowHandles()) {
					if (window.isEmpty()) {
						fullLoaded = false;
						break;
					}
				}
				return (fullLoaded);
			}
		});
		Thread.sleep(1000);
	}

	/**
	 * Wait until windows has title.
	 * 
	 * @param title
	 *            - Expected window title.
	 */
	public static void waitForTitle(String title) {
		waitForTitle(title, TIMEOUT, false);
	}

	/**
	 * Wait until windows has title.
	 * 
	 * @param timeoutSec
	 *            to wait until windows has title.
	 * @param title
	 *            - Expected window title.
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForTitle(String title, int timeoutSec,
			boolean checkCondition) {
		LOGGER.info(String.format("waitForTitle: %s", title) + "");

		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(
				DriverInit.getDriver(), timeoutSec)
				.ignoring(StaleElementReferenceException.class);
		try {
			wait.until(ExpectedConditions.titleIs(title));
		} catch (TimeoutException ignored) {
			LOGGER.info(String.format(
					"waitForTitle: [ %s ] during: [ %d ] sec ", title,
					System.currentTimeMillis() / 1000 - start));
		}
		if (checkCondition) {
			// Logic to be written
			// ReporterNGExt.logAssertEquals(//ReporterNGExt.BUSINESS_LEVEL,
			// DriverInit.getDriver().getTitle(), title, "waitForTitle",
			// TestBaseWebDriver.takePassedScreenshot);
		}
	}

	/**
	 * Wait until any element with text presents at web page.
	 * 
	 * @param text
	 *            - element text to be presents..
	 */
	public static void waitForTextToBePresent(String text) {
		waitForTextToBePresent(text, TIMEOUT, false);
	}

	/**
	 * Wait until any element with text presents at web page.
	 * 
	 * @param text
	 *            - element text to be presents.
	 * @param timeoutSec
	 *            to wait until presents.
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForTextToBePresent(String text, int timeoutSec,
			boolean checkCondition) {
		// ReporterNGExt.logAction(DriverInit.getDriver(), "",
		// String.format("waitForTextToBePresent: %s", text));
		boolean isPresent;
		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(
				DriverInit.getDriver(), timeoutSec)
				.ignoring(StaleElementReferenceException.class);
		DriverInit.setTimeout(timeoutSec);
		try {
			isPresent = wait.until(ExpectedConditions
					.textToBePresentInElementLocated(By.xpath("//*"), text));
		} catch (TimeoutException ignored) {
			
			isPresent = false;
		}
		DriverInit.setTimeout(TIMEOUT);
		if (checkCondition) {
			// code to be written
			
		}
	}

	/**
	 * Wait until any element with text not presents at web page.
	 * 
	 * @param text
	 *            - element text to not be presents.
	 */
	public static void waitForTextToNotBePresent(String text) {
		waitForTextToNotBePresent(text, TIMEOUT, false);
	}

	/**
	 * Wait until any element with text not presents at web page.
	 * 
	 * @param text
	 *            - element text to not be presents.
	 * @param timeoutSec
	 *            to wait until not presents.
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForTextToNotBePresent(String text, int timeoutSec,
			boolean checkCondition) {
		// ReporterNGExt.logAction(DriverInit.getDriver(), "",
		// String.format("waitForTextToNotBePresent: %s", text));
		boolean isNotPresent;
		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(
				DriverInit.getDriver(), timeoutSec)
				.ignoring(StaleElementReferenceException.class);
		DriverInit.setTimeout(timeoutSec);
		try {
			isNotPresent = wait.until(ExpectedConditions.not(ExpectedConditions
					.textToBePresentInElementLocated(By.xpath("//*"), text)));
		} catch (TimeoutException ignored) {
			// ReporterNGExt.logTechnical(String.format("waitForTextToNotBePresent: [ %s ] during: [ %d ] sec ",
			// text, System.currentTimeMillis() / 1000 - start));
			isNotPresent = false;
		}
		DriverInit.setTimeout(TIMEOUT);
		if (checkCondition) {
			// ReporterNGExt.logAssertTrue(//ReporterNGExt.BUSINESS_LEVEL,
			// isNotPresent,
			// String.format("waitForTextToNotBePresent: element with text '%s' should not be exists",
			// text), TestBaseWebDriver.takePassedScreenshot);
		}
	}

	/**
	 * Wait until link presents at web page.
	 * 
	 * @param linkText
	 *            - link to be presents.
	 */
	public static void waitForLinkToBePresent(String linkText) {
		waitForLinkToBePresent(linkText, TIMEOUT, false);
	}

	/**
	 * Wait until link presents at web page.
	 * 
	 * @param linkText
	 *            - linkText to be presents.
	 * @param timeoutSec
	 *            to wait until presents.
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForLinkToBePresent(String linkText, int timeoutSec,
			boolean checkCondition) {
		// ReporterNGExt.logAction(DriverInit.getDriver(), "",
		// String.format("waitForLinkToBePresent: %s", linkText));
		boolean isPresent;
		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(
				DriverInit.getDriver(), timeoutSec)
				.ignoring(StaleElementReferenceException.class);
		DriverInit.setTimeout(timeoutSec);
		try {
			wait.until(ExpectedConditions.presenceOfElementLocated(By
					.linkText(linkText)));
			isPresent = true;
		} catch (TimeoutException ignored) {
			// ReporterNGExt.logTechnical(String.format("waitForLinkToBePresent: [ %s ] during: [ %d ] sec ",
			// linkText, System.currentTimeMillis() / 1000 - start));
			isPresent = false;
		}
		DriverInit.setTimeout(TIMEOUT);
		if (checkCondition) {
			// ReporterNGExt.logAssertTrue(//ReporterNGExt.BUSINESS_LEVEL,
			// isPresent,
			// String.format("waitForLinkToBePresent: link with text '%s' should be exists",
			// linkText), TestBaseWebDriver.takePassedScreenshot);
		}
	}

	/**
	 * Wait until link not presents at web page.
	 * 
	 * @param linkText
	 *            - link to not be presents.
	 */
	public static void waitForLinkToNotBePresent(String linkText) {
		waitForLinkToNotBePresent(linkText, TIMEOUT, false);
	}

	/**
	 * Wait until link not presents at web page.
	 * 
	 * @param linkText
	 *            - linkText to not be presents.
	 * @param timeoutSec
	 *            to wait until not presents.
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForLinkToNotBePresent(String linkText,
			int timeoutSec, boolean checkCondition) {
		// ReporterNGExt.logAction(DriverInit.getDriver(), "",
		// String.format("waitForLinkToNotBePresent: %s", linkText));
		boolean isNotPresent;
		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(
				DriverInit.getDriver(), timeoutSec)
				.ignoring(StaleElementReferenceException.class);
		DriverInit.setTimeout(timeoutSec);
		try {
			isNotPresent = wait.until(ExpectedConditions
					.invisibilityOfElementLocated(By.linkText(linkText)));
		} catch (TimeoutException ignored) {
			// ReporterNGExt.logTechnical(String.format("waitForLinkToNotBePresent: [ %s ] during: [ %d ] sec ",
			// linkText, System.currentTimeMillis() / 1000 - start));
			isNotPresent = false;
		}
		DriverInit.setTimeout(TIMEOUT);
		if (checkCondition) {
			// ReporterNGExt.logAssertTrue(//ReporterNGExt.BUSINESS_LEVEL,
			// isNotPresent,
			// String.format("waitForLinkToNotBePresent: link with text '%s' should not be exists",
			// linkText), TestBaseWebDriver.takePassedScreenshot);
		}
	}

	/**
	 * Wait until Expected Condition.
	 * 
	 * @param condition
	 *            - Expected Condition
	 */
	public static void waitForExpectedCondition(
			final ExpectedCondition<Boolean> condition) {
		waitForExpectedCondition(condition, TIMEOUT, false);
	}

	/**
	 * Wait until Expected Condition.
	 * 
	 * @param condition
	 *            - Expected Condition
	 * @param timeoutSec
	 *            - the maximum time to wait in seconds
	 */
	public static void waitForExpectedCondition(
			final ExpectedCondition<Boolean> condition, final int timeoutSec) {
		waitForExpectedCondition(condition, timeoutSec, CHECKCONDITION);
	}

	/**
	 * Wait until Expected Condition.
	 * 
	 * @param condition
	 *            - Expected Condition
	 * @param timeoutSec
	 *            - the maximum time to wait in seconds
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForExpectedCondition(
			final ExpectedCondition<Boolean> condition, final int timeoutSec,
			final boolean checkCondition) {
		boolean isTrue;
		// ReporterNGExt.logAction(DriverInit.getDriver(), "",
		// String.format("waitForExpectedCondition: %s", condition));
		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = (WebDriverWait) new WebDriverWait(
				DriverInit.getDriver(), timeoutSec)
				.ignoring(StaleElementReferenceException.class);
		DriverInit.setTimeout(timeoutSec);
		try {
			wait.until(condition);
			isTrue = false;
		} catch (TimeoutException e) {
			// ReporterNGExt.logTechnical(String.format("waitForExpectedCondition: [ %s ] during: [ %d ] sec ",
			// condition, System.currentTimeMillis() / 1000 - start));
			isTrue = true;
		}
		DriverInit.setTimeout(TIMEOUT);
		if (checkCondition) {
			// ReporterNGExt.logAssertFalse(//ReporterNGExt.BUSINESS_LEVEL,
			// isTrue, String.format("waitForExpectedCondition - '%s'",
			// condition), TestBaseWebDriver.takePassedScreenshot);
		}
	}

	/**
	 * Wait until JavaScript Condition.
	 * 
	 * @param javaScript
	 *            - JavaScript Condition e.g. return (xmlhttp.readyState==4) or
	 *            (xmlhttp.status==200)
	 */
	public static void waitForJavaScriptCondition(final String javaScript) {
		waitForJavaScriptCondition(javaScript, TIMEOUT, false);
	}

	/**
	 * Wait until JavaScript Condition.
	 * 
	 * @param javaScript
	 *            - JavaScript Condition e.g. return (xmlhttp.readyState==4) or
	 *            (xmlhttp.status==200)
	 * @param timeoutSec
	 *            - the maximum time to wait in seconds
	 */
	public static void waitForJavaScriptCondition(final String javaScript,
			final int timeoutSec) {
		waitForJavaScriptCondition(javaScript, timeoutSec, CHECKCONDITION);
	}

	/**
	 * Wait until JavaScript Condition.
	 * 
	 * @param javaScript
	 *            - JavaScript Condition e.g. return (xmlhttp.readyState==4) or
	 *            (xmlhttp.status==200)
	 * @param timeoutSec
	 *            - the maximum time to wait in seconds
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForJavaScriptCondition(final String javaScript,
			final int timeoutSec, final boolean checkCondition) {
		boolean isTrue;
		// ReporterNGExt.logAction(DriverInit.getDriver(), "",
		// String.format("waitForJavaScriptCondition: %s", javaScript));
		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = new WebDriverWait(DriverInit.getDriver(),
				timeoutSec);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driverObject) {
					return (Boolean) ((JavascriptExecutor) DriverInit
							.getDriver()).executeScript(javaScript);
				}
			});
			isTrue = false;
		} catch (TimeoutException e) {
			// ReporterNGExt.logTechnical(String.format("waitForJavaScriptCondition: [ %s ] during: [ %d ] sec ",
			// javaScript, System.currentTimeMillis() / 1000 - start));
			isTrue = true;
		}
		if (checkCondition) {
			// ReporterNGExt.logAssertFalse(//ReporterNGExt.BUSINESS_LEVEL,
			// isTrue, String.format("waitForJavaScriptCondition - '%s'",
			// javaScript), TestBaseWebDriver.takePassedScreenshot);
		}
	}

	/**
	 * Wait until Ajax JQuery Process finished.
	 */
	public static void waitForAjaxJQueryProcess() {
		waitForAjaxJQueryProcess(TIMEOUT, false);
	}

	/**
	 * Wait until Ajax JQuery Process finished.
	 * 
	 * @param timeoutSec
	 *            - the maximum time to wait in seconds
	 */
	public static void waitForAjaxJQueryProcess(final int timeoutSec) {
		waitForAjaxJQueryProcess(timeoutSec, CHECKCONDITION);
	}

	/**
	 * Wait until Ajax JQuery Process finished.
	 * 
	 * @param timeoutSec
	 *            - the maximum time to wait in seconds
	 * @param checkCondition
	 *            log assert for expected conditions.
	 */
	public static void waitForAjaxJQueryProcess(final int timeoutSec,
			final boolean checkCondition) {
		boolean isTrue;
		// //ReporterNGExt.logAction(DriverInit.getDriver(), "",
		// "waitForAjaxJQueryProcess: return jQuery.active == 0");
		long start = System.currentTimeMillis() / 1000;
		WebDriverWait wait = new WebDriverWait(DriverInit.getDriver(),
				timeoutSec);
		try {
			wait.until(new ExpectedCondition<Boolean>() {
				@Override
				public Boolean apply(WebDriver driverObject) {
					return (Boolean) ((JavascriptExecutor) DriverInit
							.getDriver())
							.executeScript("return jQuery.active == 0");
				}
			});
			isTrue = false;
		} catch (TimeoutException e) {
			// ReporterNGExt.logTechnical(String.format("waitForAjaxJQueryProcess: [ return jQuery.active == 0 ] during: [ %d ] sec ",
			// System.currentTimeMillis() / 1000 - start));
			isTrue = true;
		}
		if (checkCondition) {
			// ReporterNGExt.logAssertFalse(//ReporterNGExt.BUSINESS_LEVEL,
			// isTrue, "waitForAjaxJQueryProcess - 'return jQuery.active == 0'",
			// TestBaseWebDriver.takePassedScreenshot);
		}
	}

}
