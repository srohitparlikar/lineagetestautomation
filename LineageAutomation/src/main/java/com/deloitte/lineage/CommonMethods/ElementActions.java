package com.deloitte.lineage.CommonMethods;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.deloitte.lineage.CommonMethods.LocatorFactory.LOCATOR_TYPE;

public class ElementActions extends WaitMethods {

	public static Logger logger = Logger.getLogger(ElementActions.class);
	public WebDriver driver = null;

	public ElementActions() {
		this.driver = DriverInit.getDriver();
	}

	public static boolean ClickAndWait(By locator) {
		boolean result = false;
		WebElement we = null;

		if (DriverInit.getDriver() == null) {
			logger.error("Driver is NULL");
		}

		we = getElementWithWait(locator);

		if (we != null) {
			try {
				we.click();
				result = true;
			} catch (Exception e) {
				logger.error("WebElement cannot be clicked");
				return false;
			}
		} else {
			logger.info("WebElement cannot be clicked as Element is NULL ");

			result = false;
		}

		return result;

	}

	public static boolean ClickElement(WebElement we) {
		boolean result = false;

		if (DriverInit.getDriver() == null) {
			logger.error("Driver is NULL");
		}

		if (we != null) {
			try {
				we.click();
				result = true;
			} catch (Exception e) {
				logger.error("WebElement cannot be clicked");
				return false;
			}
		} else {
			logger.info("WebElement cannot be clicked as Element is NULL ");

			result = false;
		}

		return result;

	}

	public static boolean TypeTextwithWait(By locator, String text) {
		boolean result = false;
		WebElement we = null;

		if (locator == null) {
			throw new IllegalArgumentException("Locator cannot be NULL");

		}
		if (text == null) {
			throw new IllegalArgumentException("Text cannot be NULL");
		}

		try {

			we = getElementWithWait(locator);
			if (we.isDisplayed()) {
				we.sendKeys(text);
				if (we.getText().equals(text))
					result = true;
				else
					result = false;
			}
		} catch (Exception e) {
			logger.info("Typing text into text box threw exception {}", e);
			result = false;

		}

		return result;
	}

	public static boolean TypeText(By locator, String text) {
		boolean result = false;
		WebElement we = null;

		if (locator == null) {
			throw new IllegalArgumentException("Locator cannot be NULL");

		}
		if (text == null) {
			throw new IllegalArgumentException("Text cannot be NULL");
		}

		try {

			getElement(locator).sendKeys(text);

			result = true;

		} catch (Exception e) {
			logger.info("Typing text into text box threw exception {}", e);
			result = false;

		}

		return result;
	}

	public static boolean TypeText(WebElement we, String text) {
		boolean result = false;

		if (we == null) {
			throw new IllegalArgumentException("Locator cannot be NULL");

		}
		if (text == null) {
			throw new IllegalArgumentException("Text cannot be NULL");
		}

		try {

			we.sendKeys(text);

			result = true;

		} catch (Exception e) {
			logger.info("Typing text into text box threw exception {}", e);
			result = false;

		}

		return result;
	}

	/**
	 * Select an element from drop down based on a string input
	 * 
	 * @param locator
	 * @param text
	 * @param driver
	 * @return true if element is selected from dropdown
	 * @author Shweta Parlikar
	 * @throws InterruptedException
	 */

	public static boolean SelectAndWait(By locator, String text) {
		WaitMethods.waitForElementVisible(locator);
		WebElement isPresent = DriverInit.getDriver().findElement(locator);
		Select selectElement = new Select(isPresent);
		selectElement.selectByVisibleText(text);
		if (isPresent.getText().contains(text))
			logger.info("The Element is selected");
		return true;

	}

	/**
	 * method to return webelement
	 * 
	 * @author Shweta Parlikar
	 * 
	 * @parameter By locator
	 * 
	 * @return WebElement
	 * 
	 */

	public static WebElement getElement(By locator) {
		WebElement we = null;

		if (locator == null) {
			throw new IllegalArgumentException("The locator cannot be null cannot be null.");
		}

		try {
			we = DriverInit.getDriver().findElement(locator);

		} catch (Exception e) {
			logger.warn("Getting element {} threw exception {} ", e);

		}
		return we;
	}

	/*
	 * method to return a list of webelements
	 * 
	 * @author Shweta Parlikar
	 * 
	 * @parameter webelement
	 * 
	 * @parameter tagName of the list elements
	 * 
	 * @return list of webelements
	 * 
	 */

	public static List<WebElement> getElements(WebElement we, String tagName) {

		if (we == null) {
			logger.error("WebElement cannot be NULL");
		}

		List<WebElement> elements = we.findElements(LocatorFactory.byLocator(LOCATOR_TYPE.TAG_NAME, tagName));
		return elements;

	}
	
	
	/*
	 * Method to clear text field given by the parameter
	 * @author Shweta Parlikar
	 * @parameter Webelement type locator of the field
	 * @return returns true if the field is cleared.
	 */

	public static boolean clearTextField(WebElement we) {
		boolean result = false;

		if (we == null) {
			logger.error("WebElement cannot be NULL");
			return false;
		}

		try {
			we.clear();
			result = true;
		} catch (Exception e) {
			logger.error("Cannot clear textfield ");
			result = false;
		}
		return result;
	}

}
