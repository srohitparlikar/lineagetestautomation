package com.deloitte.lineage.CommonMethods;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public final class ReadFromFile {
	
	public static  String value = null;
	public static Properties prop = new Properties();
	
	
	public static String returnValue(String filename, String variable) throws IOException,FileNotFoundException
	{
		FileInputStream fin = new FileInputStream(filename);
		prop.load(fin);
		value = prop.getProperty(variable);
		return value;
		
	}
	
	
	

}
