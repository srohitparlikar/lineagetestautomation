package com.deloitte.lineage.CommonMethods;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class LocatorFactory {
	
	/**
	 * Manage and control Locator types
	 * 
	 * @author srohitparlikar
	 */
	
	private WebDriver driver = null;
    private static Logger LOGGER = LoggerFactory.getLogger(LocatorFactory.class);
	private void LocatorFactory() {

		this.driver = DriverInit.getDriver();

	}
	
	
	public enum LOCATOR_TYPE {
		CLASS_NAME, CSS_SELECTOR, ID , LINK_TEXT, NAME, PARTIAL_LINK_TEXT, TAG_NAME, XPATH;
	};
	
	/**
	 * Method to return By Type locator based on the LOCATOR_TYPE chosen
	 * @param type - Enum controlled locator types
	 * @Param reference - value of the locator type
	 * @return By type locator 
	 */

	
	public static By byLocator(LOCATOR_TYPE type , String reference)
	{
		switch(type) {
		
		case CLASS_NAME : return By.className(reference);
		
		case CSS_SELECTOR : return By.cssSelector(reference);
		
		case ID: return By.id(reference);
		case NAME : return By.name(reference);
		case LINK_TEXT :return By.linkText(reference);
		case PARTIAL_LINK_TEXT : return By.partialLinkText(reference);
		case TAG_NAME : return By.tagName(reference);
		case XPATH : return By.xpath(reference);
	
		}
		return null;
	
		
	}
	
	

}
