package com.deloitte.lineage.PageClasses;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.deloitte.lineage.CommonMethods.DriverInit;
import com.deloitte.lineage.CommonMethods.ElementActions;
import com.deloitte.lineage.CommonMethods.LocatorFactory;
import com.deloitte.lineage.CommonMethods.LocatorFactory.LOCATOR_TYPE;
import com.deloitte.lineage.Tests.Assertion.AssertBase;

public class ChangeCasePage extends BasePage {

	String inputbox = propOR.getProperty("inputbox");
	String savebutton = propOR.getProperty("savebutton");
			
	public ChangeCasePage() throws IOException {
		super();

        if (super.driver == null) {
	        logger.warn("Driver is NULL in class ChangeCasePage");
	        this.driver = DriverInit.getDriver();
       }
	}
	
	public ChangeCasePage editCaseOwner(String caseowner) throws IOException 	{  
		
		WebDriver driver = null;
		
		AssertBase.verifyTrue(ElementActions.TypeText(LocatorFactory.byLocator(LOCATOR_TYPE.ID, inputbox), caseowner),"Case owner changed");
		logger.info("Succesfully changed case owner");
		return (new ChangeCasePage());
		
	}
	
	public CaseHome clickSaveBtn() throws IOException{
		WebElement savebtn = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.NAME, savebutton ));
 	   AssertBase.verifyTrue(ElementActions.ClickElement(savebtn),"Case Owner is changed");
 	   return (new CaseHome());
	}

}
