package com.deloitte.lineage.PageClasses;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;

import com.deloitte.lineage.CommonMethods.DriverInit;
import com.deloitte.lineage.CommonMethods.ElementActions;
import com.deloitte.lineage.CommonMethods.LocatorFactory;
import com.deloitte.lineage.CommonMethods.WaitMethods;
import com.deloitte.lineage.CommonMethods.LocatorFactory.LOCATOR_TYPE;
import com.deloitte.lineage.Tests.Assertion.AssertBase;

public class LoginPage extends BasePage {

	public static Logger LOGGER = LoggerFactory.getLogger(LoginPage.class);
	public WebDriver driver = null;
	String uname = propOR.getProperty("csr2uname");
	String pwrd = propOR.getProperty("csr2Pwd");
	String btnID = "Login";

	public LoginPage() throws IOException {
		super();
		if (driver == null) {
			LOGGER.warn("Driver is NULL in class LoginPage");
			this.driver = DriverInit.getDriver();

		}
	}

	public HomePage login(String userName, String password) throws IOException {

		try {
			AssertBase.assertTrue(ElementActions.TypeText(LocatorFactory.byLocator(LOCATOR_TYPE.ID, pwrd), password),
					"password Field is clicked");
			
			AssertBase.assertTrue(ElementActions.TypeText(LocatorFactory.byLocator(LOCATOR_TYPE.ID, uname), userName),
					"Username Field is clicked");
			

		} catch (Exception e) {
			AssertBase.addVerificationFailure(e);
		}
		

		return (new HomePage());
	}
}
