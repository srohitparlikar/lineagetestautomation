package com.deloitte.lineage.PageClasses;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.deloitte.lineage.CommonMethods.DriverInit;
import com.deloitte.lineage.CommonMethods.ElementActions;
import com.deloitte.lineage.CommonMethods.LocatorFactory;
import com.deloitte.lineage.CommonMethods.LocatorFactory.LOCATOR_TYPE;
import com.deloitte.lineage.CommonMethods.WaitMethods;
import com.deloitte.lineage.Tests.Assertion.AssertBase;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;

public class CaseHome extends BasePage {

	public static Logger logger = Logger.getLogger(HomePage.class);
	WebDriver driver = null;
	private String caseNumber = null;

	String caseNumLabel = propOR.getProperty("caseNumberLabel");
	String editBtn = propOR.getProperty("caseDetailEdit");
	String caseFeed = propOR.getProperty("caseFeed");
	String caseHistoryActField = propOR.getProperty("caseHistoryActField");
	String caseHistoryActValue = propOR.getProperty("caseHistoryActValue");
	String caseHistory = propOR.getProperty("caseHistory");
	String changelink = propOR.getProperty("changelink");

	public CaseHome() throws IOException {
		super();

		if (super.driver == null) {
			logger.warn("Driver is NULL in class HomePage");
			this.driver = DriverInit.getDriver();

		}
	}

	public CaseHome clickCaseNumber(String caseNumber) throws IOException {

		String caseNum = "//th/a[text()='" + caseNumber + "']";
		try {
			WebElement we = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseNum));

			AssertBase.assertTrue(ElementActions.ClickElement(we), "The case Number webElement is Clicked");
			;

			if (WaitMethods.waitForElementVisible(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseNumLabel)))
				;
			else
				throw new ElementNotVisibleException("Element by locator" + caseNumLabel + " is not visible ");

		} catch (Exception e) {
			logger.error("clickCaseNumber method threw exception ", e);
			AssertBase.addVerificationFailure(e);
		}
		return (new CaseHome());
	}

	public CaseDetailsPage editCase() throws IOException {

		WebElement btn = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, editBtn));
		clickEditButton(btn);

		return (new CaseDetailsPage());
	}

	/*
	 * Method to verify the updated case details
	 * 
	 * @author Shweta Parlikar
	 * 
	 * @return results of the verification as boolean
	 * 
	 */

	public boolean verifyAuditTrail(String fieldChanged, String expectedValue) throws Exception {

		clickCaseHistory();

		boolean result = false;
		WebElement field1 = ElementActions
				.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseHistoryActField));
		String actualField = field1.getText();

		if (fieldChanged.equalsIgnoreCase(actualField)) {

			AssertBase.assertEquals(actualField, fieldChanged);
			result = true;
			logger.info("Actual Field is same as expected field");
		} else {
			result = false;
			logger.error("Field in audit trail history is not as expected");
			AssertBase.fail("Failed as Actual Field is not same as expected field");
		}
		WebElement field2 = ElementActions
				.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseHistoryActValue));
		String actualValue = field2.getText();
		if (actualValue.matches("(.*)" + expectedValue + "(.*)")) {
			AssertBase.assertEquals(actualValue, expectedValue);
			result = true;
			logger.info("Actual value is same as expected value");
		} else {
			result = false;
			logger.error("Field value in audit trail history is not as expected");
			AssertBase.fail("Failed as Actual value is not same as expected value");
		}

		return result;

	}

	public ChangeCasePage clickCaseOwnerChangeLink() throws IOException {

		WebElement changeLink = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.LINK_TEXT, changelink));
		AssertBase.verifyTrue(ElementActions.ClickElement(changeLink), "Change Owner link is clicked");
		return (new ChangeCasePage());

	}

	public void clickCaseHistory() throws IOException {
		try {
			WebElement we = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseHistory));
			AssertBase.verifyTrue(ElementActions.ClickElement(we), "Case history is clicked");
		}

		catch (Exception e) {
			logger.info(e);
		}
	}

	public void clickEditButton(WebElement btn) {
		AssertBase.verifyTrue(ElementActions.ClickElement(btn), "Edit Button is clicked");
	}
	
	
	public boolean verifyNonAuditTrailFields(String fieldChanged, String expectedValue) throws Exception {
        
        clickCaseHistory();
        
        boolean result = false;
        WebElement actfield = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseHistoryActField));
        String actualField = actfield.getText();
        
        if (!(fieldChanged.equalsIgnoreCase(actualField))) {
           AssertBase.assertTrue(!(fieldChanged.equalsIgnoreCase(actualField)));
                       
        } else {
        	AssertBase.fail("Test case failed because actual field and expected field were matching");
        	
        }
        
        return result;

    }


}
