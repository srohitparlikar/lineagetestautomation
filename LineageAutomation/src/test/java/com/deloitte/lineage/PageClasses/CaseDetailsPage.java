package com.deloitte.lineage.PageClasses;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.deloitte.lineage.CommonMethods.DriverInit;
import com.deloitte.lineage.CommonMethods.ElementActions;
import com.deloitte.lineage.CommonMethods.LocatorFactory;
import com.deloitte.lineage.CommonMethods.LocatorFactory.LOCATOR_TYPE;
import com.deloitte.lineage.Tests.Assertion.AssertBase;

/**
 * CaseDetailsPage class to expose Case Details page functionality
 * 
 * @author Shweta Parlikar
 */
public class CaseDetailsPage extends BasePage {
	public static Logger logger = Logger.getLogger(CaseDetailsPage.class);
	WebDriver driver = null;
	String caseSubject = propOR.getProperty("caseSubject");
	String saveBtn = propOR.getProperty("saveBtn");
	String statusDropDown = propOR.getProperty("statusDropDown");
	String reasonCode = propOR.getProperty("reasonCode");
	String date = propOR.getProperty("date");
	String caseFacility = propOR.getProperty("caseFacility");
	String caseDescription = propOR.getProperty("caseDescription");
    String caseCarrier = propOR.getProperty("caseCarrier");
    String caseOrigin = propOR.getProperty("caseOrigin");
    String parentCase = propOR.getProperty("parentCase");
    String orderNo = propOR.getProperty("orderNo");
   

	public CaseDetailsPage() throws IOException {
		super();

		if (super.driver == null) {
			logger.warn("Driver is NULL in class CasePage");
			this.driver = DriverInit.getDriver();
		}
	}

	public CaseDetailsPage editCaseSubject(String subject) throws Exception {
		long currentTimeStamp = System.currentTimeMillis();
		String str = String.valueOf(currentTimeStamp);
		WebElement sub = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseSubject));
		AssertBase.verifyTrue(ElementActions.clearTextField(sub), "Cleared subject field");
		AssertBase.verifyTrue(ElementActions.TypeText(sub, subject + str), "Typed Text inside subject element");
		String newText = sub.getText();

		logger.info("New field is  " + newText + "Succesfully edited inside Subject element");
		return (new CaseDetailsPage());

	}

	public CaseDetailsPage editStatus(String statusOption) throws IOException {
		logger.info("Selecting status option from case details page "+statusOption);

		AssertBase.verifyTrue(ElementActions.SelectAndWait(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, statusDropDown),
				statusOption), "status option changed");

		return (new CaseDetailsPage());
	}

	public CaseHome clickSaveBtn() throws IOException {
		WebElement btn = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, saveBtn));
		AssertBase.verifyTrue(ElementActions.ClickElement(btn), "Clicked Save Button");
		logger.info("Clicked save button on Case Details Page");
		return (new CaseHome());
	}

	public CaseDetailsPage editCaseReason(String reasonOption) throws Exception {
		logger.info("Editing Case Reason with option" + reasonOption);
		WebElement we = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, reasonCode));
		Select sel = new Select (we);
		sel.selectByValue(reasonOption);

	/*	AssertBase.assertTrue(
				ElementActions.SelectAndWait(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, reasonCode), reasonOption),
				"Selected reason code option");*/

		logger.info("Successfully selected option "+reasonOption);

		return (new CaseDetailsPage());
	}
	
	public CaseDetailsPage editCaseDate(String dateInput) throws Exception {
		logger.info("Editing Case date with date" + dateInput);
		

		AssertBase.assertTrue(
				ElementActions.TypeText(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, date), dateInput),
				"Selected reason code option");

		logger.info("Successfully edited date with  "+ dateInput);

		return (new CaseDetailsPage());
	}

	
	
	
	public CaseDetailsPage editCaseDescription() throws Exception
    {   long currentTimeStamp = System.currentTimeMillis();
          String str = String.valueOf(currentTimeStamp);;
          WebElement desc = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseDescription));
          String existText = desc.getText();
          AssertBase.verifyTrue(ElementActions.TypeText(desc, existText+str),"Typed text into description");
          logger.info("Succesfully entered text in description");
          return (new CaseDetailsPage());
          
    }
    
    public CaseDetailsPage editCaseCarrier(String carrier) throws Exception
    {   WebElement cc = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseCarrier));
          cc.clear();
          AssertBase.verifyTrue(ElementActions.TypeText(cc, carrier ),"Carrier selected");
          logger.info("Succesfully changed carrier");
          return (new CaseDetailsPage());
          
    }

    public CaseDetailsPage editParentCase(String parentcase) throws Exception
    {   WebElement pc = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.ID, parentCase));
          pc.clear();
          AssertBase.verifyTrue(ElementActions.TypeText(pc, parentcase),"Parent Case Selected");
          logger.info("Succesfully changed parent case");
          return (new CaseDetailsPage());
          
    }

    public CaseDetailsPage editCaseOrigin(String ddvalue) throws Exception
    {   
          
          AssertBase.verifyTrue(ElementActions.SelectAndWait(LocatorFactory.byLocator(LOCATOR_TYPE.ID, caseOrigin), ddvalue),"Case Origin Selected");
          logger.info("Succesfully selected Case Origin");
          return (new CaseDetailsPage());
          
    }
    
    public CaseDetailsPage enterOrderNo(String orderno) throws Exception
    {   
          WebElement on = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, orderNo));
          AssertBase.verifyTrue(ElementActions.TypeText(on, orderno),"order no entered");
          logger.info("Succesfully entered order number");
          return (new CaseDetailsPage());
          
    }
    
    public CaseDetailsPage editCaseFacility(String casefacility) throws Exception
    {   WebElement cf = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, caseFacility));
          cf.clear();
          AssertBase.verifyTrue(ElementActions.TypeText(cf, casefacility ),"Facility Changed");
          logger.info("Succesfully changed facility");
          return (new CaseDetailsPage());
          
    }


}
