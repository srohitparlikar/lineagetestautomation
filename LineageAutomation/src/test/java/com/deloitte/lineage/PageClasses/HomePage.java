package com.deloitte.lineage.PageClasses;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.deloitte.lineage.CommonMethods.DriverInit;
import com.deloitte.lineage.CommonMethods.ElementActions;
import com.deloitte.lineage.CommonMethods.LocatorFactory;
import com.deloitte.lineage.CommonMethods.LocatorFactory.LOCATOR_TYPE;
import com.deloitte.lineage.CommonMethods.WaitMethods;
import com.deloitte.lineage.Tests.Assertion.AssertBase;

public class HomePage extends BasePage {

	public static Logger logger = Logger.getLogger(HomePage.class);
	public String tabBar = propOR.getProperty("tabBar");
	public String casesLabel = propOR.getProperty("casesLabel");
	private String casesTab = "Cases";
	private String var1 = "tabBar";
	private String varOption = "Cases";
	private String tabOption = "//*[@id='"+var1+"']/li/a[text()='"+varOption+"']";



	private WebDriver driver = null;

	public HomePage() throws IOException {
		super();

		if (super.driver == null) {
			logger.warn("Driver is NULL in class HomePage");
			this.driver = DriverInit.getDriver();

		}
	}

	public CaseHome clickCaseTab() throws IOException {

		logger.info("Clicking tab Cases ");
		By locator = LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, casesLabel);
		
		try{
			
			WebElement we = ElementActions.getElement(LocatorFactory.byLocator(LOCATOR_TYPE.XPATH, tabOption));
			
		     AssertBase.assertTrue(ElementActions.ClickElement(we), "Webelement on tab bar is clicked");
		
		     WaitMethods.waitForElementPresent(locator);
		
				}catch(Exception e){
			AssertBase.addVerificationFailure(e);
		}
		return (new CaseHome());
	}

	public WebElement getTabName(By locator, String strTab) {
		List<WebElement> elements = null;

		try {
			
			WebElement tabBar = ElementActions.getElement(locator);
			elements = ElementActions.getElements(tabBar,"li");
			for (WebElement we : elements) {
				if (we.getText().equalsIgnoreCase(strTab)) {
					return we;
				}
			}

		} catch (Exception e) {
			logger.error("Caught exception e", e);

		}
		return null;

	}

}
