package com.deloitte.lineage.testClasses;

import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import org.testng.annotations.Test;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.deloitte.lineage.CommonMethods.DriverInit;
import com.deloitte.lineage.CommonMethods.WebDriverFunctions;
import com.deloitte.lineage.PageClasses.LoginPage;
import com.deloitte.lineage.Tests.Assertion.AssertBase;

public class TestStory265 extends TestBase {
	private WebDriver driver = null;
	public String site = propConfig.getProperty("siteURL");
	private String browser = propConfig.getProperty("browser");

	public static Logger LOGGER = Logger.getLogger(TestStory265.class);

	@BeforeMethod
	public void beforeTestPreRequisites() {
		DriverInit dIni = new DriverInit();
		dIni.beforeSuite(browser);

		this.driver = DriverInit.getDriver();

	}

	@AfterMethod
	public void afterTestTearDowns() {
		LOGGER.info("Tearing down browser window");
		WebDriverFunctions.quit();

	}

	/**
	 * 
	 * Test to edit the case subject on the case details page
	 * 
	 * @author Shweta Parlikar
	 * @throws IOException
	 */

	@Test(priority = 1)
	public void TC_LLVP_265_1() throws IOException {

		driver.get(site);
		int rownum = em.getCellRowNum("TestData", "Testcase", "TC_LLVP_265_1");
		String userName = em.getCellData("TestData", "Username", rownum);
		String password = em.getCellData("TestData", "Password", rownum);
		String caseNumber = em.getCellData("TestData", "CaseNumber", rownum);
		String caseSubject = em.getCellData("TestData", "CaseSubject", rownum);
		String changedField = em.getCellData("TestData", "Verification", rownum);

		LoginPage lp = new LoginPage();
		LOGGER.info("The logs are generated for test case TC_LLVP_265_1_ edit the case subject ");
		try {
			lp.login(userName, password).clickCaseTab().clickCaseNumber(caseNumber).editCase()
					.editCaseSubject(caseSubject).clickSaveBtn();

		} catch (Exception e) {
			AssertBase.addVerificationFailure(e);
		}

	}

	/**
	 * Track Case Owner field for Audit Trail change
	 * 
	 * @author Avtar Jha
	 */
	@Test(priority = 7)
	public void TC_LLVP_265_2() throws IOException {

		driver.get(site);
		int rownum = em.getCellRowNum("TestData", "Testcase", "TC_LLVP_265_2");
		String userName = em.getCellData("TestData", "Username", rownum);
		String password = em.getCellData("TestData", "Password", rownum);
		String caseNumber = em.getCellData("TestData", "CaseNumber", rownum);
		String caseowner = em.getCellData("TestData", "CaseOwner", rownum);
		String expectedField = em.getCellData("TestData", "Verification", rownum);

		LoginPage lp = new LoginPage();
		LOGGER.info("The logs for TC_LLVP_265_2() are generated here");
		try {
			lp.login(userName, password).clickCaseTab().clickCaseNumber(caseNumber).clickCaseOwnerChangeLink()
					.editCaseOwner(caseowner).clickSaveBtn().verifyAuditTrail(expectedField, caseowner);

		}

		catch (Exception e) {
			AssertBase.addVerificationFailure(e);
		}

	}

	/**
	 * Track Facility field for Audit Trail change
	 * 
	 * @author Avtar jha
	 */

	@Test(priority = 2)
	public void TC_LLVP_265_3() throws IOException {

		driver.get(site);
		int rownum = em.getCellRowNum("TestData", "Testcase", "TC_LLVP_265_3");
		String userName = em.getCellData("TestData", "Username", rownum);
		String password = em.getCellData("TestData", "Password", rownum);
		String caseNumber = em.getCellData("TestData", "CaseNumber", rownum);
		String casefacility = em.getCellData("TestData", "CaseFacility", rownum);
		String expectedField = em.getCellData("TestData", "Verification", rownum);

		LoginPage lp = new LoginPage();
		LOGGER.info("The logs for TC_LLVP_265_3() are generated here");
		try {
			lp.login(userName, password).clickCaseTab().clickCaseNumber(caseNumber).editCase()
					.editCaseFacility(casefacility).clickSaveBtn().verifyAuditTrail(expectedField, casefacility);

		}

		catch (Exception e) {
			AssertBase.addVerificationFailure(e);
		}

	}

	/**
	 * Test to track changes in non audit trail fields
	 * 
	 * @author Avtar jha
	 * @throws IOException
	 */

	@Test(priority = 2)
	public void TC_LLVP_265_4() throws IOException {

		driver.get(site);
		int rownum = em.getCellRowNum("TestData", "Testcase", "TC_LLVP_265_4");
		String userName = em.getCellData("TestData", "Username", rownum);
		String password = em.getCellData("TestData", "Password", rownum);
		String caseNumber = em.getCellData("TestData", "CaseNumber", rownum);
		String caseOrigin = em.getCellData("TestData", "CaseOrigin", rownum);
		String carrier = em.getCellData("TestData", "Carrier", rownum);
		String parentCase = em.getCellData("TestData", "ParentCase", rownum);
		String orderNo = em.getCellData("TestData", "OrderNo", rownum);
		String expectedField = em.getCellData("TestData", "Verification", rownum);

		LoginPage lp = new LoginPage();
		LOGGER.info("The logs for TC_LLVP_265_4() are generated here");
		try {
			lp.login(userName, password).clickCaseTab().clickCaseNumber(caseNumber).editCase()
					.editParentCase(parentCase)
					// .editCaseOrigin(caseOrigin) these fields coming as part
					// of sprint 2 audit trail
					// .editCaseCarrier(carrier) these fields coming as part of
					// sprint 2 audit trail
					.editCaseDescription()
					// .enterOrderNo(orderNo) these fields coming as part of
					// sprint 2 audit trail
					.clickSaveBtn().verifyNonAuditTrailFields(expectedField, ""); // Avtar
																					// to
																					// provide
																					// code

		} catch (Exception e) {
			AssertBase.addVerificationFailure(e);
		}

	}

	/**
	 * Test to edit the status field on Case Details Page
	 * 
	 * @throws IOException
	 */

	@Test(priority = 3)
	public void TC_LLVP_265_5() throws IOException {

		driver.get(site);
		int rownum = em.getCellRowNum("TestData", "Testcase", "TC_LLVP_265_5");
		String userName = em.getCellData("TestData", "Username", rownum);
		String password = em.getCellData("TestData", "Password", rownum);
		String caseNumber = em.getCellData("TestData", "CaseNumber", rownum);
		String caseStatus = em.getCellData("TestData", "CaseStatus", rownum);
		String changedField = em.getCellData("TestData", "Verification", rownum);

		LoginPage lp = new LoginPage();
		LOGGER.info("The logs are generated for test case TC_LLVP_265_5_ edit the case status ");
		try {
			lp.login(userName, password).clickCaseTab().clickCaseNumber(caseNumber).editCase().editStatus(caseStatus)
					.clickSaveBtn().verifyAuditTrail(changedField, caseStatus);
		} catch (Exception e) {
			AssertBase.addVerificationFailure(e);
		}

	}

	/**
	 * Test to edit the Reason field on Case Details Page
	 * 
	 * @throws IOException
	 */

	@Test(priority = 3)
	public void TC_LLVP_265_6() throws IOException {

		driver.get(site);
		int rownum = em.getCellRowNum("TestData", "Testcase", "TC_LLVP_265_6");
		String userName = em.getCellData("TestData", "Username", rownum);
		String password = em.getCellData("TestData", "Password", rownum);
		String caseNumber = em.getCellData("TestData", "CaseNumber", rownum);
		String reasonOption = em.getCellData("TestData", "ReasonOption", rownum);
		String changedField = em.getCellData("TestData", "Verification", rownum);

		LoginPage lp = new LoginPage();
		LOGGER.info("The logs are generated for test case TC_LLVP_265_6_ edit the case reason ");
		try {
			lp.login(userName, password).clickCaseTab().clickCaseNumber(caseNumber).editCase()
					.editCaseReason(reasonOption).clickSaveBtn().verifyAuditTrail(changedField, reasonOption);
		} catch (Exception e) {
			LOGGER.error("The test threw an exception ", e);

			AssertBase.addVerificationFailure(e);
		}

	}

	/**
	 * Test to edit the DATE field on Case Details Page
	 * 
	 * @throws IOException
	 */

	@Test(priority = 4)
	public void TC_LLVP_265_7() throws IOException {

		driver.get(site);
		int rownum = em.getCellRowNum("TestData", "Testcase", "TC_LLVP_265_6");
		String userName = em.getCellData("TestData", "Username", rownum);
		String password = em.getCellData("TestData", "Password", rownum);
		String caseNumber = em.getCellData("TestData", "CaseNumber", rownum);
		String dateInput = em.getCellData("TestData", "Date", rownum);
		String changedField = em.getCellData("TestData", "Verification", rownum);

		LoginPage lp = new LoginPage();
		LOGGER.info("The logs are generated for test case TC_LLVP_265_7_ edit the case date ");
		try {
			lp.login(userName, password).clickCaseTab().clickCaseNumber(caseNumber).editCase().editCaseDate(dateInput)
					.clickSaveBtn().verifyNonAuditTrailFields(changedField, dateInput);
		} catch (Exception e) {
			LOGGER.error("The test threw an exception ", e);

			AssertBase.addVerificationFailure(e);
		}

	}

}
