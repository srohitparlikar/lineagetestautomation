package com.deloitte.lineage.testClasses;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import com.deloitte.lineage.CommonMethods.ExcelMethods;

public class TestBase {
	
	public String excelpath = System.getProperty("user.dir") + "\\src\\main\\java\\resources\\TestDataSheet.xlsx";
	
	public File configFile = new File(System.getProperty("user.dir")+ "\\src\\main\\java\\resources\\Config.properties");
	public ExcelMethods em = null;
	
	public Properties propConfig = new Properties();
	
	
	public TestBase() 
	{
		try{
			
		
		FileInputStream finConfig = new FileInputStream(configFile);
		propConfig.load(finConfig);
		
		
		em = new ExcelMethods(excelpath);
		}catch(FileNotFoundException fnfe)
		{
			//implement log4j instead of slf4j
			System.out.println("FileNotFoundException fnfe");
		}
		
		catch(IOException ioe)
		{
			//implement log4j instead of slf4j
			System.out.println("IOException ioe");
		}
	}
	
	
	
		
	

}
